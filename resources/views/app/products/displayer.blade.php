<div class="app-product-displayer">
    <a href=" {{ route('product_show',['product'=>$p,'slug'=>$p->slug]) }} " class="m-link">
        <div class="image-container" style="background-image:url('{{ $p->image }}')">
            <div class="status">
                    <span class="success"> {{ $p->type }} </span>
                
            </div>
        </div>
        <div class="details-container">
            <div class="title-c">{{ $p->name }}</div>
            <div class="price">{{ $p->price }} <small><i class="fas fa-euro-sign    "></i></small></div>
            <div class="date">HS : {{ $p->reference }} </div>

        </div>
    </a>
</div>
