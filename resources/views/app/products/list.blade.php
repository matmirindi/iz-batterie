@extends('layouts.master')

@section('content')

<div class="py-4">
    <div class="container">
        <div class="product-list">
            <div class="s-wrapper">
                <h4 class="s-title">Nos produits</h4>
                <div class="content">
                    @foreach ($products as $p)
                        @include('app.products.displayer',['p'=>$p])
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>
    
@endsection