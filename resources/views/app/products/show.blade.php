@extends('layouts.master')

@section('content')

    <div class="product-show">
        <div class="wrapper">
            <div class="row">
                <div class="col-md-6">
                    <div class="image-container">
                        <img src="{{ $product->image }}" alt="">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="details-container">
                        <div class="name">{{ $product->name }}</div>
                        <div class="type">Type : <span>{{ $product->type }}</span> </div>
                        <div class="price">{!! number_format($product->price,0,'.',' ') !!} <small><i class="fas fa-euro-sign    "></i></small></div>
                        <div class="description"> {{ $product->description }} </div>
                        <div class="order">
                            <a href="tel:22133823651" class="btn btn-success"> <i class="fas fa-phone    "></i> Appeler</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection