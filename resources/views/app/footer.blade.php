<div class="master-footer">
    @if(false)
    <div class="newsletter-container">
        <div class="wrapper">
            <div class="row">
                <div class="col-md-6 mb-2">
                    <h2 class="title">Abonnez-vous à notre newsletter</h2>
                    <h5 class="caption">Restez connectés avec nous</h5>
                </div>
                <div class="col-md-6">
                    <form action="" method="post">
                        <div class="form-group">
                            <div class="input-group">
                                <input class="form-control" type="email" name="" placeholder="Votre e-mail" required>
                                <div class="input-group-append">
                                    <button type="submit" class="btn btn-default">S'abonner</button>
                                    {{--  '  --}}
                                </div>
                            </div>
                            @include("utilities.input-error",['name'=>"email"])
                        </div>
                    </form>

                </div>
            </div>

        </div>
    </div>
    @endif
    <div class="footer-content">
        <div class="wrapper">
            <div class="upper-f row">
                <div class="section">
                    <div class="s-title">A propos</div>
                    <div class="menus-container">
                        <a href="">A propos</a>
                    </div>
                </div>
                <div class="section">
                    <div class="s-title">Liens utiles</div>
                    <div class="menus-container">
                        <a href="">Nos produits</a>
                        <a href="">Nous contacter</a>
                    </div>
                </div>
                <div class="section">
                    <div class="s-title">Suivez-nous</div>
                    <div class="social-medias">
                        <a href=""><i class="fab fa-facebook-f  fa-2x  "></i></a>
                        {{--  <i href=""><i class="fab fa-twitter   fa-2x   "></i></i>  --}}
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="credits">
        <div class="wrapper">
            <div class="row">
                <div class="col-md-8">
                    Tous droit reservé © {{ config('app.name') }} | developer par <a href="https://www.10gitalizer.com" target="_blank" class="text-underline">10gitalizer</a></div>
                    <div class="col-md-4"><a href=""><i class="fas fa-envelope    "></i> contact@izbatterie.com</a></div>
            </div>

        </div>
    </div>
</div>
