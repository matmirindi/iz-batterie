@extends('layouts.master')

@section('content')

    
    <div class="app-idx-page">
        <section class="hiro">
            <div class="s-wrapper ">
                <div class="flex-row-small-wrap">
                    <div>
                        <div class="caption-side">
                            <div class="title-c">
                                <h3>Iz Batterie</h3>
                                <h5 class="subtitle">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. </h5>
                            </div>
                            <div class="action">
                                <a href="" class="btn btn-primary rounded-button ">Nos produits</a>
                            </div>
    
                        </div>
                    </div>
                </div>
    
            </div>
        </section>
        <section class="product-list">
            <div class="s-wrapper">
                <h4 class="s-title">Nos produits</h4>
                <div class="content">
                    @for($x =0; $x<25;$x++)
                        @foreach ($products as $p)
                            @include('app.products.displayer',['p'=>$p])
                        @endforeach
                    @endfor
                </div>
            </div>
        </section>
    </div>

@endsection