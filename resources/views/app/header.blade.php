
@auth
    <div class="p-2 " style="background-color:#000">

        <a href=" {{ route('ad_products') }} " style='color:#fff'> Administration </a>

    </div>    
@endauth
<div class="master-header">
    <div class="hdr--wrapper">
        <div class="logo-container">
            <a href="{{ route('home') }}">
                <img src="{{ asset('images/logo.png') }}" alt="logo">
            </a>
        </div>
        <div class="menu-container">
            <div class="menu m-menu"><a href=" {{ route('products') }} ">Nos produits</a></div>
            <div class="menu m-menu"><a href="">A propos</a></div>
            <div class="menu m-menu"><a href="" title="Nous contacter">Contact</a></div>
            <div class="menu small-menu"><a href="" onclick="event.preventDefault();$('#small-menu-hdr').toggle()"><i class="fas fa-bars fa-2x   "></i></a></div>
        </div>
    </div>
    <div class="small-menu" id="small-menu-hdr">
        <div class="menu m-menu"><a href=" {{ route('products') }} ">Nos produits</a></div>
        <div class="menu m-menu"><a href="">A propos</a></div>
        <div class="menu m-menu"><a href="" title="Nous contacter">Contact</a></div>
    </div>
    {{--  notifications  --}}
    @include('app.utilities.notifications')
</div>
