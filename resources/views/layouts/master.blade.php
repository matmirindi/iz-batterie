<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>


        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        {{--  seo  --}}
        {{-- @yield('seo', View::make('app.seo')) --}}

        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title> @yield('title',"Iz Batterie") </title>
         {{-- icon --}}
         <link rel="icon" href="{{ asset('favicon.png') }}">

        <!-- Scripts -->
        <script src="{{ mix('/js/app.js') }}" defer></script>



        <!-- Styles -->
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
        <link href="{{ mix('/css/app.css') }}" rel="stylesheet">

    </head>
<body>

    <div id="app">

        {{-- header --}}
        @include('app.header')
        {{--  --}}
        <div id="app-content">
            @yield('content')
        </div>
        @include("app.footer")
    </div>
</body>
</html>
