<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>

    <meta name="robots" content="noindex, nofollow">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title> @yield('title',"Iz Batterie") </title>
     {{-- icon --}}
     <link rel="icon" href="{{ asset('favicon.png') }}">

    <!-- Scripts -->
    <script src="{{ mix('/js/admin.js') }}" defer></script>



    <!-- Styles -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
    <link href="{{ mix('/css/admin.css') }}" rel="stylesheet">

</head>
<body>
    <div id="app">
        <main id="app-main">
            <div class="ad-layout">
                <div class="menus-container">
                    {{--  dashboard  --}}
                     <div class="menu">
                        <div><a href="{{ route('ad_products') }}" class="m-link @if($menu_name == 'products') active @endif"><i class="fas fa-list-alt    "></i></a></div>

                    </div> 
                    
                    
                    {{--  contacts  --}}
                    <div class="menu">
                            <div><a href="" class="m-link @if($menu_name == 'contacts') active @endif" title="Messages"><i class="fas fa-envelope-open"></i></a></div>
                    </div>
                    {{--  users  --}}
                    <div class="menu">
                            <div><a href="" class="m-link @if($menu_name == 'users') active @endif"><i class="fas fa-users    "></i></a></div>
                    </div>

                </div>
                <div class="content-section">
                    <div class="master-hdr">
                        <div class="wrapper">
                            {{-- profile --}}
                            <div class="profile-container">
                                <div class="name">Bonjour, {{ Auth::user()->name }}</div>
                            </div>
                            {{-- logout --}}
                            <div class="logout-container">
                                    <a class="" href="{{ route('logout') }}"
                                    onclick="event.preventDefault();
                                                  document.getElementById('logout-form').submit();">
                                    <i class="fas fa-sign-out-alt    "></i> {{ __('Logout') }}
                                 </a>

                                 <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                     @csrf
                                 </form>
                            </div>
                            {{--  back home  --}}
                            <div>
                                <div><a href="{{ route('home') }}"><i class="fas fa-forward    "></i> Page d'acceuil</a></div>
                            </div>
                        </div>
                    </div>
                    <div class="master-content">
                        {{-- notifications --}}
                        <div class="notifications">
                            @include("admin.utilities.notifications")
                        </div>

                        {{-- content --}}
                        <div class="content">
                            @yield('content')
                        </div>
                    </div>
                </div>
            </div>
        </main>
    </div>
    @yield("scripts")
</body>
</html>
