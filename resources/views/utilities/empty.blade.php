<div class="is-empty-p">
    <div class="e-wrapper">
        <img src="{{ asset('images/empty.svg') }}" alt="">
        <div>
            <h6 class="text-center py-3 text-muted">Aucun resultat</h6>
        </div>
    </div>

</div>
