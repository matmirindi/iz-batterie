
            {{--  #infos notifications  --}}
            @if(Session::has('info'))
                <div class="alert alert-info">
                    <h5 class="main-title"><i class="fas fa-exclamation-circle    "></i> {{ Session::get('info') }} </h5>
                </div>
            @endif
            {{--  #warning  notifications  --}}
            @if(Session::has('warning'))
                <div class="alert alert-warning">
                    <h5 class="main-title"><i class="fas fa-exclamation-triangle    "></i> {{ Session::get('warning') }} </h5>
                </div>
            @endif
            {{--  #errors   notifications  --}}
            @if(Session::has('error'))
                <div class="alert alert-danger">
                    <h5 class="main-title"> <i class="fas fa-times-circle    "></i> {{ Session::get('error') }} </h5>
                </div>
            @endif
            {{--  #success  notifications  --}}
            @if(Session::has('success'))
                <div class="alert alert-success">
                    <h5 class="main-title"> <i class="fas fa-check-circle    "></i> {{ Session::get('success') }} </h5>
                </div>
            @endif
