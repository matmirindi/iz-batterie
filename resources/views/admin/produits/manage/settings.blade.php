@extends('admin.produits.layout')

@section('f-content')

<div class="white-box">
    <div class="card">
        <div class="card-header">Supprimer la formation</div>
        <div class="card-body">
            <div class="alert alert-warning">la suppression d'un produit est irreversible. pour supprimer un produit utiliser le boutton ci-dessous.</div>
            {{--  '  --}}
            <div class="form-group text-right">
                <a href="" class="btn btn-danger" onclick="event.preventDefault();document.getElementById('delete').submit();">Fermer le compte</a>
                <form action="{{ route('ad_products_settings_delete',['product'=>$product]) }}" method="post" id="delete" style="display:none">
                    @csrf
                </form>
            </div>
        </div>
    </div>
</div>


@endsection