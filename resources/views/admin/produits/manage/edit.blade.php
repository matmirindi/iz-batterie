@extends('admin.produits.layout')

@section('f-content')

    <div class="white-box">
        <form action="{{ route('ad_products_update',['product'=>$product]) }}" method="post" enctype="multipart/form-data">
            @csrf
            <div class="form-group">
                <label for="">Type</label>
                <input type="text" name="type" id="" class="form-control" value="{{ $product->type }}" >
                @include('utilities.input-error',['name'=>'type'])
            </div>
            <div class="form-group">
                <label for="">Nom du produit</label>
                <input type="text" name="nom" id="" class="form-control" value="{{ $product->type }}" placeholder="Ex : BATT.TESTER ">
                @include('utilities.input-error',['name'=>'nom'])
            </div>
            <div class="form-group">
                <label for="">Reference</label>
                <input type="text" name="reference" id="" class="form-control" value="{{ $product->reference }}" placeholder="Ex : 85049099 ">
                @include('utilities.input-error',['name'=>'reference'])
            </div>
            <div class="form-group">
                <label for="">Prix ( <small><i class="fas fa-euro-sign    "></i></small> )</label>
                <input type="number" name="prix" id="" class="form-control" value="{{ $product->price }}" placeholder="ex : 150 ">
                @include('utilities.input-error',['name'=>'prix'])
            </div>
            <div class="form-group row">
                <div class="col-md-6">
                    <label for="">Image actuelle</label>
                    <img src="{{ $product->image }}" alt="" class="img-thumbnail" style="width:100%">
                </div>
                <div class="col-md-6">
                    <label for="image">Modifier l'image</label>
                    {{--  '  --}}
                    <input type="file" name="image" id="image" class="form-control" accept="image/*">
                    @include('utilities.input-error',['name'=>'image'])
                </div>

            </div>
            <div class="form-group">
                <label for="" class="optional">Description</label>
                <textarea name="description" class="form-control row-100">{{ $product->description }}</textarea>
                @include('utilities.input-error',['name'=>'description'])
            </div>
            <div class="form-group text-right">
                <button type="submit" class="btn btn-info">Mettre à jour</button>
            </div>
        </form>
    </div>

@endsection
