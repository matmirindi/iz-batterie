@extends('layouts.admin')

@section('content')

<div class="u-hdr row mb-3">
    <div class="col-md-6 mb-2">
        <div class="search-bar">
            <form action="{{ route('ad_products') }}" method="get">
                <div class="form-group">
                    <input type="search" class="form-control" name="q" placeholder="Rechercher un produit" value="{{ $q }}">
                </div>
            </form>
        </div>
    </div>
    <div class="col-md-6 mb-2">
        <div class="text-right">
            <a href="{{ route('ad_products_create') }}" class="btn btn-primary"><i class="fas fa-plus    "></i></a>
        </div>
    </div>

</div>

@if(sizeOf($products) > 0)

    <div class="ad-products-list">
        @foreach ($products as $p)
            @include("admin.produits.displayer",['p'=>$p])
        @endforeach
    </div>

<div class="form-group">
    {{ $products->links() }}
</div>

@else
@include('utilities.empty')
@endif

    
@endsection