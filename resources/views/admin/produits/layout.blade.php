@extends('layouts.admin')

@section('content')

    <div class="ad-u-m-layout">
        <div class="row">
            <div class="col-md-3 mb-2">
                <div class="menu-side">
                    <a href="{{ route('ad_product_edit',['product'=>$product]) }}" class="@if($section == 'edit') active @endif">Informations</a>
                    <a href="{{ route('ad_product_settings',['product'=>$product]) }}" class="@if($section == 'settings') active @endif">Paramètres</a>

                </div>

            </div>
            <div class="col-md-9 mb-2">
                <div>
                    @yield('f-content')
                </div>
            </div>
        </div>

    </div>

@endsection
