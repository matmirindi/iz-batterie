@extends('layouts.admin')

@section('content')

    <div class="create--form--001">
        <div class="wrapper">
            <div class="mb-3">
                <a href=" {{ route('ad_products') }} " class="text-info"><i class="fas fa-chevron-left    "></i> Tous les produits</a>
            </div>
            <div class="title-container">Nouveau produit</div>
            <div class="bdy">
                <form action="{{ route('ad_products_store') }}" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group">
                        <label for="">Type</label>
                        <input type="text" name="type" id="" class="form-control" value="{{ old('type') }}" placeholder="Ex : BAT/ 48294">
                        @include('utilities.input-error',['name'=>'type'])
                    </div>
                    <div class="form-group">
                        <label for="">Nom du produit</label>
                        <input type="text" name="nom" id="" class="form-control" value="{{ old('nom') }}" placeholder="Ex : BATT.TESTER ">
                        @include('utilities.input-error',['name'=>'nom'])
                    </div>
                    <div class="form-group">
                        <label for="">Reference</label>
                        <input type="text" name="reference" id="" class="form-control" value="{{ old('reference') }}" placeholder="Ex : 85049099 ">
                        @include('utilities.input-error',['name'=>'reference'])
                    </div>
                    <div class="form-group">
                        <label for="">Prix ( <small><i class="fas fa-euro-sign    "></i></small> )</label>
                        <input type="number" name="prix" id="" class="form-control" value="{{ old('prix') }}" placeholder="ex : 150 ">
                        @include('utilities.input-error',['name'=>'prix'])
                    </div>
                    <div class="form-group">
                        <label for="">Image</label>
                        <input type="file" name="image" id="" class="form-control"  accept="image/*">
                        @include('utilities.input-error',['name'=>'image'])
                    </div>
                    <div class="form-group">
                        <label for="" class="optional">Description</label>
                        <textarea name="description" class="form-control row-100">{{ old('description') }}</textarea>
                        @include('utilities.input-error',['name'=>'description'])
                    </div>
                    <div class="form-group text-right">
                        <button type="submit" class="btn btn-primary">Enregistrer</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection
