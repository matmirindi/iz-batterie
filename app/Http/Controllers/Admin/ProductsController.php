<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Product;
use App\Traits\ImageHelper;

class ProductsController extends Controller
{
    use ImageHelper;
    //
    public $menu_name = "products";

    public function index(Request $request)
    {
        $q = $request->get('q');
        $menu_name = $this->menu_name;
        $products = Product::where('name','LIKE','%'.$q.'%')->paginate(25);

        return view('admin.produits.index',compact('menu_name','q','products'));
    }

    //
    public function create(Request $request)
    {
        $q = $request->get('q');
        $menu_name = $this->menu_name;

        return view('admin.produits.create',compact('menu_name','q'));
    }

    //
    public function store(Request $request)
    {
        $request->validate([
            "type"=>'required|string|max:255',
            "nom"=>"required|string|max:255",
            "reference"=>"required|string|unique:products,reference",
            "prix"=>"required|numeric|min:0",
            "image"=>"required|image|mimes:png,jpg,jpeg",
            "description"=>"nullable|string",
        ]);
        $p = new Product;
        $p->type = $request->get('type');
        $p->name = $request->get('nom');
        $p->slug = str_slug($p->name);
        $p->reference = $request->get('reference');
        $p->price = $request->get('prix');
        $p->image = $this->saveImage($request->file('image'),"/images/produits/");
        $p->description = $request->get('description');
        if($p->save())
        {
            return redirect()->back()->with('success',$p->name.' a été ajouté avec succès');
        }
        return redirect()->back()->withInput()->with('success',"Une erreur s'est produite lors de l'enregistrement de ce produit, veuillez réessayer");
    }

    //
    public function edit(Product $product)
    {
        $menu_name = $this->menu_name;
        $section = 'edit';
        return view('admin.produits.manage.edit',compact('menu_name','section','product'));
    }
    //
    public function update(Request $request,Product $product)
    {
        $request->validate([
            "type"=>'required|string|max:255',
            "nom"=>"required|string|max:255",
            "reference"=>"required|string",
            "prix"=>"required|numeric|min:0",
            "image"=>"nullable|image|mimes:png,jpg,jpeg",
            "description"=>"nullable|string",
        ]);
        $p = $product;
        $p->type = $request->get('type');
        $p->name = $request->get('nom');
        $p->slug = str_slug($p->name);
        $p->reference = $request->get('reference');
        $p->price = $request->get('prix');
        if($request->get('image'))
        {
            $p->image = $this->saveImage($request->file('image'),"/images/produits/");
        }
        $p->description = $request->get('description');
        if($p->update())
        {
            return redirect()->back()->with('success',$p->name.' a été mise à jour avec succès');
        }
        return redirect()->back()->withInput()->with('success',"Une erreur s'est produite lors de la mise à jour de ce produit, veuillez réessayer");
    }

    //
    public function settings(Request $request,Product $product)
    {
        $menu_name = $this->menu_name;
        $section = "settings";
        return view("admin.produits.manage.settings",compact('menu_name','section','product'));
    }

    //
    public function delete(Request $request,Product $product)
    {
        $product->delete();
        return redirect()->route('ad_products')->with("success",$product->name." a été supprimé avec scuccès");
    }

}
