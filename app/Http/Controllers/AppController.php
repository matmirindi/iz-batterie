<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Http\Request;

class AppController extends Controller
{
    //
    public function index()
    {
        $products = Product::get();
        return view('app.index',compact('products'));
    }
    //
    public function productShow(Product $product)
    {
        return view("app.products.show",compact('product'));
    }

    //
    public function products(Request $request)
    {
        $q = $request->get('q');
        $products = Product::get();
        return view('app.products.list',compact('products'));
    }
}
