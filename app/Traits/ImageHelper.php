<?php

namespace App\Traits;

use Image;

trait ImageHelper
{
    //
    public function saveImage($file,$path)
    {
        // save image
        if (!file_exists(public_path($path)))
        {
           mkdir(public_path($path), 0777, true);
        }
        $imgName = time().'sima-sima'.random_int(1000000, 9999999).'.jpg';
        $img = Image::make($file);
        $img->save(public_path($path.$imgName));
        $image = asset($path.$imgName);
        return $image;
    }
}