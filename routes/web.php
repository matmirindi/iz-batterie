<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();


Route::get("/","AppController@index")->name('home');
//
Route::get('/produits/{product}/{slug}',"AppController@productShow")->name('product_show');
//
Route::get('/produits','AppController@products')->name('products');



/*
 *
 * ADMIN
 * 
 * 
 */
Route::group(['prefix' => 'admin','middleware'=>'auth','namespace'=>'Admin'], function () {
    

    //
    Route::group(['prefix' => 'produits'], function () {
        //
        Route::get('/','ProductsController@index')->name('ad_products');
        //
        Route::get('/create','ProductsController@create')->name('ad_products_create');
        //
        Route::post('/store','ProductsController@store')->name('ad_products_store');

        //
        Route::group(['prefix' => 'manage/{product}'], function () {
            //
            Route::get('/edit','ProductsController@edit')->name('ad_product_edit');
            //
            Route::post('/update','ProductsController@update')->name('ad_products_update');
            //
            Route::get('/settings','ProductsController@settings')->name('ad_product_settings');
            Route::post('/settings/delete','ProductsController@delete')->name('ad_products_settings_delete');
        });
    });
}); 